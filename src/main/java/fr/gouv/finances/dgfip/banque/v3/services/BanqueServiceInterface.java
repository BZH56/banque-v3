package fr.gouv.finances.dgfip.banque.v3.services;

import fr.gouv.finances.dgfip.banque.v3.CompteException;
import fr.gouv.finances.dgfip.banque.v3.entites.Banque;
import fr.gouv.finances.dgfip.banque.v3.entites.CarteBancaire;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteCourant;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteEpargne;
import fr.gouv.finances.dgfip.banque.v3.entites.Personne;

public interface BanqueServiceInterface {

	public Banque creerBanque(String banque) throws CompteException;

	public CompteCourant creerCompteCourant(Banque banque, Personne titulaire, String guichet, Double soldeInitial)
			throws CompteException;

	public CompteCourant creerCompteCourant(Banque banque, Personne titulaire, String guichet) throws CompteException;

	public CompteEpargne creerCompteEpargne(Banque banque, Personne titulaire, String guichet, Double soldeInitial,
			Double taux) throws CompteException;

	public CarteBancaire creerCarte(Banque banque, Personne titulaire, CompteCourant compte) throws CompteException;

	public void afficherSyntheseComptes(Banque banque);

	public void afficherSyntheseCartes(Banque banque);

}
