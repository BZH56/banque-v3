package fr.gouv.finances.dgfip.banque.v3.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.dgfip.banque.v3.BanqueV3Application;
import fr.gouv.finances.dgfip.banque.v3.dao.PersonneDao;
import fr.gouv.finances.dgfip.banque.v3.entites.Personne;
import fr.gouv.finances.dgfip.banque.v3.services.PersonneServiceInterface;

@Service
public class PersonneService implements PersonneServiceInterface {

	private static final Logger LOGGER = LoggerFactory.getLogger(BanqueV3Application.class);
	private PersonneDao personneDao;

	@Override
	public Personne creerPersonne(String nom, String prenom) {

		String paddedNom = String.format("%-23s", nom);
		String paddedPrenom = String.format("%-23s", prenom);
		LOGGER.info("| " + paddedNom + " | " + paddedPrenom + " |");
		return personneDao.save(new Personne(nom, prenom));
	}

	@Autowired
	public void setPersonneDao(PersonneDao personneDao) {
		this.personneDao = personneDao;
	}

}
