package fr.gouv.finances.dgfip.banque.v3.entites;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class CompteCourant extends CompteBancaire {

	@OneToMany(mappedBy = "compteCourant")
	private Set<CarteBancaire> setCarte = new HashSet<CarteBancaire>();

	/********************************/

	public CompteCourant() {
		super();
	}

	public CompteCourant(String codeBanque, String codeGuichet, String numCompte, String cle, Double solde) {
		super(codeBanque, codeGuichet, numCompte, cle, solde);
	}

	public CompteCourant(UUID id, String codeBanque, String codeGuichet, String numCompte, String cle, Double solde,
			Banque banque, Personne titulaire, List<Operation> operations, int numeroOperation) {
		super(id, codeBanque, codeGuichet, numCompte, cle, solde, banque, titulaire, operations, numeroOperation);
		// TODO Auto-generated constructor stub
	}

	public Double calculerSolde() {
//  LOGGER.info("calculerSolde");
		Double solde = this.solde;
		for (int i = 0; i < this.operations.size(); i++) {
			solde += this.operations.get(i).getMontant();
		}
		return solde;
	}

	/********************************/
	public void addCarte(CarteBancaire newCarteBancaire) {
		setCarte.add(newCarteBancaire);
	}

	/**
	 * @return the setCarte
	 */
	public Set<CarteBancaire> getSetCarte() {
		return setCarte;
	}

	/**
	 * @param setCarte the setCarte to set
	 */
	public void setSetCarte(Set<CarteBancaire> setCarte) {
		this.setCarte = setCarte;
	}

	public CompteCourant(String codeBanque, String codeGuichet, String numCompte, String cle, Double solde,
			Set<CarteBancaire> setCarte) {
		super(codeBanque, codeGuichet, numCompte, cle, solde);
		this.setCarte = setCarte;
	}

}
