package fr.gouv.finances.dgfip.banque.v3.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.dgfip.banque.v3.BanqueV3Application;
import fr.gouv.finances.dgfip.banque.v3.CompteException;
import fr.gouv.finances.dgfip.banque.v3.dao.BanqueDao;
import fr.gouv.finances.dgfip.banque.v3.dao.CarteBancaireDao;
import fr.gouv.finances.dgfip.banque.v3.dao.CompteBancaireDao;
import fr.gouv.finances.dgfip.banque.v3.dao.PersonneDao;
import fr.gouv.finances.dgfip.banque.v3.entites.Banque;
import fr.gouv.finances.dgfip.banque.v3.entites.CarteBancaire;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteCourant;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteEpargne;
import fr.gouv.finances.dgfip.banque.v3.entites.Personne;
import fr.gouv.finances.dgfip.banque.v3.services.BanqueServiceInterface;
import fr.gouv.finances.dgfip.banque.v3.services.CompteBancaireServiceInterface;

@Service
public class BanqueService implements BanqueServiceInterface {

	private static final Logger LOGGER = LoggerFactory.getLogger(BanqueV3Application.class);
	private CompteBancaireServiceInterface compteBancaireService;
	private BanqueDao banqueDao;
	private CompteBancaireDao compteBancaireDao;
	private CarteBancaireDao carteBancaireDao;

	@Autowired
	private PersonneDao personneDao;

	public Banque creerBanque(String banque) throws CompteException {

		Banque maBanque = new Banque(banque);
		Random random = new Random();
		int nb;
		nb = random.nextInt(99999);
		String codeBanque = String.format("%05d", nb);
		String paddedMaBanque = String.format("%-23s", banque);
		String paddedCodeGuichet = String.format("%-5s", codeBanque);

		maBanque.setCodeBanque(codeBanque);

		LOGGER.info("| " + paddedMaBanque + " | " + paddedCodeGuichet + " | ");

		return banqueDao.save(maBanque);
	}

	public CompteCourant creerCompteCourant(Banque banque, Personne titulaire, String codeGuichet, Double soldeInitial)
			throws CompteException {

		Random random = new Random();
		int NumCompte = banque.getNumCompte();
		String numComptePadded = String.format("%010d", NumCompte);
		banque.setNumCompte(NumCompte + 1);
		int nb;
		nb = random.nextInt(99);
		String cle = String.format("%02d", nb);
		CompteCourant newCompteCourant = new CompteCourant(banque.getCodeBanque(), codeGuichet, numComptePadded, cle,
				soldeInitial);
		newCompteCourant.setBanque(banque);

		newCompteCourant.setTitulaire(titulaire);
		// Personne personneInDB = personneDao.save(titulaire);
//		newCompteCourant.setTitulaire(personneInDB);

		titulaire.addCompteBancaire(newCompteCourant);
		banque.getSetCompteBancaire().add(newCompteCourant);

//		banque.setMapCompteAPersonne();

		compteBancaireDao.save(newCompteCourant);
		return newCompteCourant;
	}

	public CompteCourant creerCompteCourant(Banque banque, Personne titulaire, String guichet) throws CompteException {
		return this.creerCompteCourant(banque, titulaire, guichet, 0.0);
	}

	public CompteEpargne creerCompteEpargne(Banque banque, Personne titulaire, String codeGuichet, Double soldeInitial,
			Double taux) throws CompteException {

		Random random = new Random();
		int NumCompte = banque.getNumCompte();
		String numComptePadded = String.format("%010d", NumCompte);
		banque.setNumCompte(NumCompte + 1);
		int nb;
		nb = random.nextInt(99);
		String cle = String.format("%02d", nb);

		CompteEpargne newCompteEpargne = new CompteEpargne(banque.getCodeBanque(), codeGuichet, numComptePadded, cle,
				soldeInitial, taux);
		newCompteEpargne.setBanque(banque);
		newCompteEpargne.setTitulaire(titulaire);

		titulaire.addCompteBancaire(newCompteEpargne);
		banque.getSetCompteBancaire().add(newCompteEpargne);

		compteBancaireDao.save(newCompteEpargne);
		return newCompteEpargne;
	}

	public CarteBancaire creerCarte(Banque banque, Personne titulaire, CompteCourant compteCourant)
			throws CompteException {

		List<String> randStr = new ArrayList<String>();
		for (int nb : new Random().ints(4, 0, 10000).toArray()) {
			randStr.add(String.format("%04d", nb));
		}
		String numCarte = String.join(" ", randStr);
		String codePin = "5613";
		Calendar calendar = new GregorianCalendar();
		int yearPlus3 = calendar.get(Calendar.YEAR) + 3;
		calendar.set(Calendar.YEAR, yearPlus3);
		Date dateExpiration = calendar.getTime();
		CarteBancaire newCarteBancaire = new CarteBancaire(codePin, numCarte, dateExpiration);
		newCarteBancaire.setBanque(banque);
		newCarteBancaire.setCompteCourant(compteCourant);
		newCarteBancaire.setTitulaire(titulaire);
		banque.addCarte(newCarteBancaire);
		titulaire.addCarte(newCarteBancaire);
		compteCourant.addCarte(newCarteBancaire);
		carteBancaireDao.save(newCarteBancaire);
		return newCarteBancaire;
	}

	public void afficherSyntheseComptes(Banque banque) {
		LOGGER.info("+-----------------+------------------------------+----------------------+------------+");
		LOGGER.info("| Type compte     | RIB                          | Titulaire            | Solde      |");
		LOGGER.info("+-----------------+------------------------------+----------------------+------------+");

		for (CompteBancaire compte : banque.getSetCompteBancaire()) {
			Personne titulaire = compte.getTitulaire();
			String compteType = compte instanceof CompteCourant ? "Compte courant" : "Compte epargne";
			String paddedCompteType = String.format("%-15s", compteType);
			String paddedRib = String.format("%-28s", compte.getRib());
			String fullName = titulaire.getNom() + " " + titulaire.getPrenom();
			String paddedFullName = String.format("%-20s", fullName);
			String paddedSolde = String.format("%10.2f", compte.calculerSolde());
			LOGGER.info(
					"| " + paddedCompteType + " | " + paddedRib + " | " + paddedFullName + " | " + paddedSolde + " |");
		}
		LOGGER.info("+-----------------+------------------------------+----------------------+------------+");
	}

	public void afficherSyntheseCartes(Banque banque) {
		LOGGER.info("+----------------------+----------------------+----------+");
		LOGGER.info("| Titulaire            | Num. Carte           | Code PIN |");
		LOGGER.info("+----------------------+----------------------+----------+");

		for (CarteBancaire carte : banque.getSetCarte()) {
			Personne titulaire = carte.getTitulaire();
			String fullName = titulaire.getNom() + " " + titulaire.getPrenom();
			String paddedFullName = String.format("%-21s", fullName);
			String paddedNumCarte = String.format("%-21s", carte.getNumCarte());
			String paddedPin = String.format("%-8s", carte.getCodePin());

			LOGGER.info("| " + paddedFullName + "| " + paddedNumCarte + "| " + paddedPin + " |");
		}
		LOGGER.info("+----------------------+----------------------+----------+");
	}

	public CarteBancaire lierCarte(Banque banque, CarteBancaire carte, CompteCourant compteCourant) {
		Personne titulaire = compteCourant.getTitulaire();

		carte.setBanque(banque);
		carte.setCompteCourant(compteCourant);
		carte.setTitulaire(titulaire);

		titulaire.addCarte(carte);
		compteCourant.addCarte(carte);
		// banque.addCarte(carte);
		return carte;
	}

	@Autowired
	public void setCompteBancaireService(CompteBancaireServiceInterface compteBancaireService) {
		this.compteBancaireService = compteBancaireService;
	}

	@Autowired
	public void setBanqueDao(BanqueDao banqueDao) {
		this.banqueDao = banqueDao;
	}

	@Autowired
	public void setCompteBancaireDao(CompteBancaireDao compteBancaireDao) {
		this.compteBancaireDao = compteBancaireDao;
	}

	@Autowired
	public void setCarteBancaireDao(CarteBancaireDao carteBancaireDao) {
		this.carteBancaireDao = carteBancaireDao;
	}

}