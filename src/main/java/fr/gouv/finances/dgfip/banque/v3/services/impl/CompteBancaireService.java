package fr.gouv.finances.dgfip.banque.v3.services.impl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.dgfip.banque.v3.BanqueV3Application;
import fr.gouv.finances.dgfip.banque.v3.CompteException;
import fr.gouv.finances.dgfip.banque.v3.dao.CompteBancaireDao;
import fr.gouv.finances.dgfip.banque.v3.dao.OperationDao;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteEpargne;
import fr.gouv.finances.dgfip.banque.v3.entites.Operation;
import fr.gouv.finances.dgfip.banque.v3.entites.Personne;
import fr.gouv.finances.dgfip.banque.v3.services.CompteBancaireServiceInterface;

@Service
public class CompteBancaireService implements CompteBancaireServiceInterface {

	private static final Logger LOGGER = LoggerFactory.getLogger(BanqueV3Application.class);
	private OperationDao operationDao;
	private CompteBancaireDao compteBancaireDao;

	@Override
	@Transactional
	public int creerOperation(CompteBancaire compte, String libelle, Double montant) throws CompteException {
		if (compte instanceof CompteEpargne) {
			if (compte.calculerSolde() + montant < 0) {
				throw new CompteException("Operation impossible : solde négatif");
			}
		}
//		int numeroOperation = compte.getNumeroOperation();
//		Operation operation = new Operation(numeroOperation, new Date(), libelle, montant);
//		operation.setCompte(compte);
//		compte.getOperations().add(operation);
//		operationDao.save(operation);
//		compte.setNumeroOperation(numeroOperation + 1);
//		compteBancaireDao.save(compte);
//		return numeroOperation + 1;

		CompteBancaire b = compteBancaireDao.findById(compte.getId()).get();
		int numeroOperation = b.getNumeroOperation();
		Operation operation = new Operation(numeroOperation, new Date(), libelle, montant);
		operation.setCompte(compte);
		b.getOperations().add(operation);
		operationDao.save(operation);
		b.setNumeroOperation(numeroOperation + 1);
		compteBancaireDao.save(b);
		compte.setOperations(b.getOperations());
		return numeroOperation + 1;

	}

//	@Override
//	public int creerOperation(Banque banque, CompteBancaire compte, String libelle, Double montant)
//			throws CompteException {
//		if (compte instanceof CompteEpargne) {
//			if (compte.calculerSolde() + montant < 0) {
//				throw new CompteException("Operation impossible : solde négatif");
//			}
//		}
//		int numeroOperation = compte.getNumeroOperation();
//		Operation operation = new Operation(numeroOperation, new Date(), libelle, montant);
//		compte.getOperations().add(operation);
//		compte.setNumeroOperation(numeroOperation + 1);
//		return numeroOperation;
//	}

	@Override
	public void afficherSyntheseOperations(CompteBancaire compte) {
//	    compte.afficherSyntheseOperations();

		LOGGER.info("");
		LOGGER.info("Synthèse du compte: " + compte.getCodeBanque() + " " + compte.getCodeGuichet() + " "
				+ compte.getNumCompte() + " " + compte.getCle());
		Personne titulaire = compte.getTitulaire();
		LOGGER.info("Titulaire: " + titulaire.getNom() + " " + titulaire.getPrenom());

		LOGGER.info("+---------+-------------------------+-------------------------+------------+");
		LOGGER.info("| Num opé | Date opération          | Libellé                 | Montant    |");
		LOGGER.info("+---------+-------------------------+-------------------------+------------+");

		afficherSyntheseOperation(0, new Date(), "SOLDE INITIAL", compte.getSolde());
		for (Operation op : compte.getOperations()) {
			Integer numOperation = op.getNumOperation();
			Date dateOperation = op.getDateOperation();
			String libelle = op.getLibelle();
			Double montant = op.getMontant();

			afficherSyntheseOperation(numOperation, dateOperation, libelle, montant);
		}
		LOGGER.info("+---------+-------------------------+-------------------------+------------+");
	}

	private void afficherSyntheseOperation(Integer numOperation, Date dateOperation, String libelle, Double montant) {

		DecimalFormat df = new DecimalFormat("0.00");
		String paddedNumOperation = String.format("%7d", numOperation);
		String date = new SimpleDateFormat("yyyy-MM-dd").format(dateOperation) + "T"
				+ new SimpleDateFormat("HH:mm:ss").format(dateOperation);
		String paddedDate = String.format("%-23s", date);
		String paddedLibelle = String.format("%-23s", libelle);
		String paddedMontant = String.format("%10s", df.format(montant));
		LOGGER.info(
				"| " + paddedNumOperation + " | " + paddedDate + " | " + paddedLibelle + " | " + paddedMontant + " |");
	}

	@Override
	public double calculerInteret(CompteEpargne compte) {
		return compte.calculerInterets();
	}

	@Autowired
	public void setOperationDao(OperationDao operationDao) {
		this.operationDao = operationDao;
	}

	@Autowired
	public void setCompteBancaireDao(CompteBancaireDao compteBancaireDao) {
		this.compteBancaireDao = compteBancaireDao;
	}

}
