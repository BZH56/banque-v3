package fr.gouv.finances.dgfip.banque.v3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BanqueV3Application {

	private static final Logger LOGGER = LoggerFactory.getLogger(BanqueV3Application.class);

	public static void main(String[] args) throws Throwable {
		ApplicationContext context = SpringApplication.run(BanqueV3Application.class, args);
	}

}
