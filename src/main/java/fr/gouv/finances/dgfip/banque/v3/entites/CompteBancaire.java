package fr.gouv.finances.dgfip.banque.v3.entites;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.dgfip.banque.v3.BanqueV3Application;

@Entity
public abstract class CompteBancaire {

	private static final Logger LOGGER = LoggerFactory.getLogger(BanqueV3Application.class);
	@Id
	@GeneratedValue // (strategy = GenerationType.IDENTITY)
	private UUID id;

//	protected String codeBanque;
//	protected String codeGuichet;
//	protected String numCompte;
//	protected String cle;
//	protected Double solde;
//  @NotNull
	protected String codeBanque;

	@NotBlank(message = "Code guichet ?")
	protected String codeGuichet;

	@NotBlank(message = "Numero de compte ?")
	protected String numCompte;

	@NotBlank(message = "Clé ?")
	protected String cle;

	@Min(value = 0, message = "Solde positif ?")
	protected Double solde;

	@ManyToOne
	private Banque banque;
	@ManyToOne
	private Personne titulaire;

	/********************************/
	@OneToMany // (mappedBy = "compte")
	protected List<Operation> operations = new ArrayList<Operation>();

	private int numeroOperation = 1;

	/********************************/

	public CompteBancaire() {
		super();
		this.codeBanque = "";
		this.codeGuichet = "";
		this.numCompte = "";
		this.cle = "";
		this.solde = 0.;
	}

	public CompteBancaire(String codeBanque, String codeGuichet, String numCompte, String cle, Double solde) {
		this.codeBanque = codeBanque;
		this.codeGuichet = codeGuichet;
		this.numCompte = numCompte;
		this.cle = cle;
		this.solde = solde;
	}

	/********************************/
	public Banque getBanque() {
		return banque;
	}

	public void setBanque(Banque banque) {
		this.banque = banque;
	}

	public Personne getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(Personne titulaire) {
		this.titulaire = titulaire;
	}

	/********************************/
	public abstract Double calculerSolde();

	public void afficherSyntheseOperations() {
		LOGGER.info("");
		LOGGER.info("Synthèse du compte: " + this.codeBanque + " " + this.codeGuichet + " " + this.numCompte + " "
				+ this.cle);
		Personne titulaire = this.titulaire;
		LOGGER.info("Titulaire: " + titulaire.getNom() + " " + titulaire.getPrenom());

		LOGGER.info("+---------+-------------------------+-------------------------+------------+\n"
				+ "| Num opé | Date opération          | Libellé                 | Montant    |\n"
				+ "+---------+-------------------------+-------------------------+------------+");

		afficherSyntheseOperation(0, new Date(), "SOLDE INITIAL", this.solde);

		for (Operation op : operations) {
			Integer numOperation = op.getNumOperation();
			Date dateOperation = op.getDateOperation();
			String libelle = op.getLibelle();
			Double montant = op.getMontant();

			afficherSyntheseOperation(numOperation, dateOperation, libelle, montant);
		}
		LOGGER.info("+---------+-------------------------+-------------------------+------------+");
	}

	private void afficherSyntheseOperation(Integer numOperation, Date dateOperation, String libelle, Double montant) {
		String paddedNumOperation = String.format("%7d", numOperation);
		String date = new SimpleDateFormat("yyyy-MM-dd").format(dateOperation) + "T"
				+ new SimpleDateFormat("HH:mm:ss").format(dateOperation);
		String paddedDate = String.format("%-23s", date);
		String paddedLibelle = String.format("%-23s", libelle);
		String paddedMontant = String.format("%10s", montant);
		LOGGER.info(
				"| " + paddedNumOperation + " | " + paddedDate + " | " + paddedLibelle + " | " + paddedMontant + " |");
	}

	public String getRib() {
		return String.format("%s %s %s %s", this.codeBanque, this.codeGuichet, this.numCompte, this.cle);
	}

	/**
	 * @return the operations
	 */
	public List<Operation> getOperations() {
		return operations;
	}

	/**
	 * @param operations the operations to set
	 */
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	/**
	 * @return the numeroOperation
	 */
	public int getNumeroOperation() {
		return numeroOperation;
	}

	/**
	 * @param numeroOperation the numeroOperation to set
	 */
	public void setNumeroOperation(int numeroOperation) {
		this.numeroOperation = numeroOperation;
	}

	/**
	 * @return the codeBanque
	 */
	public String getCodeBanque() {
		return codeBanque;
	}

	/**
	 * @return the codeGuichet
	 */
	public String getCodeGuichet() {
		return codeGuichet;
	}

	/**
	 * @return the numCompte
	 */
	public String getNumCompte() {
		return numCompte;
	}

	/**
	 * @return the cle
	 */
	public String getCle() {
		return cle;
	}

	/**
	 * @return the solde
	 */
	public Double getSolde() {
		return solde;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the logger
	 */
	public static Logger getLogger() {
		return LOGGER;
	}

	public CompteBancaire(UUID id, String codeBanque, String codeGuichet, String numCompte, String cle, Double solde,
			Banque banque, Personne titulaire, List<Operation> operations, int numeroOperation) {
		super();
		this.id = id;
		this.codeBanque = codeBanque;
		this.codeGuichet = codeGuichet;
		this.numCompte = numCompte;
		this.cle = cle;
		this.solde = solde;
		this.banque = banque;
		this.titulaire = titulaire;
		this.operations = operations;
		this.numeroOperation = numeroOperation;
	}

	/**
	 * @param codeBanque the codeBanque to set
	 */
	public void setCodeBanque(String codeBanque) {
		this.codeBanque = codeBanque;
	}

	/**
	 * @param codeGuichet the codeGuichet to set
	 */
	public void setCodeGuichet(String codeGuichet) {
		this.codeGuichet = codeGuichet;
	}

	/**
	 * @param numCompte the numCompte to set
	 */
	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}

	/**
	 * @param cle the cle to set
	 */
	public void setCle(String cle) {
		this.cle = cle;
	}

	/**
	 * @param solde the solde to set
	 */
	public void setSolde(Double solde) {
		this.solde = solde;
	}

}
