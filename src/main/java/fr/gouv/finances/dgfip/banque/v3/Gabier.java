package fr.gouv.finances.dgfip.banque.v3;

import org.springframework.stereotype.Component;

import fr.gouv.finances.dgfip.banque.v3.entites.Banque;

@Component
public class Gabier {

	private final SystemeBancaireInterface systemeBancaire;

	public Gabier(SystemeBancaireInterface systemeBancaire) {
		this.systemeBancaire = systemeBancaire;
	}

	public String accesComptes(Banque banque, String numCarte, String codePin) throws SystemeBancaireException {
		if (this.systemeBancaire.verifierCodePin(banque, numCarte, codePin)) {
			return this.systemeBancaire.rechercheRIBCompteCarte(banque, numCarte);
		} else {
			throw new SystemeBancaireException("Carte ne peut pas être vérifiée");
		}
	}

	public int retirerEspeces(Banque banque, String ribCompte, Double montant) throws SystemeBancaireException {
		int numOperation = systemeBancaire.creerOperation(banque, ribCompte, "Retirer gabier", -montant);
		return numOperation - 1;
	}
}
