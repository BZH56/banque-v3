package fr.gouv.finances.dgfip.banque.v3;

import java.util.List;

import fr.gouv.finances.dgfip.banque.v3.entites.Banque;
import fr.gouv.finances.dgfip.banque.v3.entites.Personne;

public interface SystemeBancaireInterface {
//	String rechercheRIBCompteCarte(String numCarte) throws SystemeBancaireException;

	String rechercheRIBCompteCarte(Banque banque, String numCarte) throws SystemeBancaireException;

//	Boolean verifierCodePin(String numCarteCompte, String codePin) throws SystemeBancaireException;

	Boolean verifierCodePin(Banque banque, String numCarte, String codePin) throws SystemeBancaireException;

	Integer creerOperation(Banque banque, String ribCompte, String libelle, Double montant)
			throws SystemeBancaireException;

	List<Personne> listeAdherent(Banque banque);

}
