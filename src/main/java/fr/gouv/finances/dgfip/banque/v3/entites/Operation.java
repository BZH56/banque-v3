package fr.gouv.finances.dgfip.banque.v3.entites;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Operation {

	@Id
	@GeneratedValue // (strategy = GenerationType.IDENTITY)
	private UUID id;

	private final Integer numOperation;
	private final Date dateOperation;
	private final String libelle;
	private final Double montant;

	@ManyToOne
	private CompteBancaire compte;

	public Operation() {
		super();
		this.numOperation = 0;
		this.dateOperation = new Date();
		this.libelle = "";
		this.montant = 0.;
	}

	public Operation(Integer numOperation, Date dateOperation, String libelle, Double montant) {
		this.numOperation = numOperation;
		this.dateOperation = dateOperation;
		this.libelle = libelle;
		this.montant = montant;
	}

	/**
	 * @return the numOperation
	 */
	public Integer getNumOperation() {
		return numOperation;
	}

	/**
	 * @return the dateOperation
	 */
	public Date getDateOperation() {
		return dateOperation;
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @return the montant
	 */
	public Double getMontant() {
		return montant;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the compte
	 */
	public CompteBancaire getCompte() {
		return compte;
	}

	/**
	 * @param compte the compte to set
	 */
	public void setCompte(CompteBancaire compte) {
		this.compte = compte;
	}

}
