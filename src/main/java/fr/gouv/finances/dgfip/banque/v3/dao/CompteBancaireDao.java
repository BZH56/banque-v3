package fr.gouv.finances.dgfip.banque.v3.dao;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import fr.gouv.finances.dgfip.banque.v3.entites.Banque;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteBancaire;

//@Repository
public interface CompteBancaireDao extends CrudRepository<CompteBancaire, UUID> {

	Iterable<CompteBancaire> findByBanque(Banque banque);

	CompteBancaire findByCodeGuichetAndNumCompte(String codeGuichet, String numCompte);

}
