package fr.gouv.finances.dgfip.banque.v3.services.impl;

import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.dgfip.banque.v3.CompteException;
import fr.gouv.finances.dgfip.banque.v3.SystemeBancaireException;
import fr.gouv.finances.dgfip.banque.v3.SystemeBancaireInterface;
import fr.gouv.finances.dgfip.banque.v3.dao.CarteBancaireDao;
import fr.gouv.finances.dgfip.banque.v3.dao.CompteBancaireDao;
import fr.gouv.finances.dgfip.banque.v3.dao.PersonneDao;
import fr.gouv.finances.dgfip.banque.v3.entites.Banque;
import fr.gouv.finances.dgfip.banque.v3.entites.CarteBancaire;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v3.entites.Personne;
import fr.gouv.finances.dgfip.banque.v3.services.CompteBancaireServiceInterface;

@Service
public class SystemeBancaire implements SystemeBancaireInterface {

	@Autowired
	private CompteBancaireServiceInterface compteBancaireService;
	private CarteBancaireDao carteBancaireDao;
	private CompteBancaireDao compteBancaireDao;
	private PersonneDao personneDao;

	@Override
	public String rechercheRIBCompteCarte(Banque banque, String numCarte) throws SystemeBancaireException {
		String rib = "";
		for (CarteBancaire carte : banque.getSetCarte()) {
			if (carte.getNumCarte().equals(numCarte)) {
				rib = carte.getCompteCourant().getRib();
			}
		}
		if (!rib.isEmpty()) {
			return rib;
		} else {
			throw new SystemeBancaireException("RIB non trouvé");
		}
	}

	@Override
	public Integer creerOperation(Banque banque, String ribCompte, String libelle, Double montant)
			throws SystemeBancaireException {

		CompteBancaire compteTrouve = null;
		for (CompteBancaire compte : banque.getSetCompteBancaire()) {
			if (compte.getRib().equals(ribCompte)) {
				compteTrouve = compte;
				break;
			}
		}

		if (compteTrouve != null) {
			try {
				return compteBancaireService.creerOperation(compteTrouve, libelle, montant);
			} catch (CompteException e) {
				throw new SystemeBancaireException(e.getMessage());
			}
		} else
			throw new SystemeBancaireException("Compte non trouvé");
	}

	@Override
	public Boolean verifierCodePin(Banque banque, String numCarte, String codePin) throws SystemeBancaireException {
		boolean ok = false;
		for (CarteBancaire carte : banque.getSetCarte()) {
			if ((carte.getCodePin().equals(codePin)) && (carte.getNumCarte().equals(numCarte))) {
				ok = true;
			}
		}
		if (ok) {
			return ok;
		} else {
			throw new SystemeBancaireException("Carte non trouvée");
		}
	}

	@Autowired
	public void setCompteBancaireService(CompteBancaireServiceInterface compteBancaireService) {
		this.compteBancaireService = compteBancaireService;
	}

	@Autowired
	public void setCarteBancaireDao(CarteBancaireDao carteBancaireDao) {
		this.carteBancaireDao = carteBancaireDao;
	}

	@Autowired
	public void setCompteBancaireDao(CompteBancaireDao compteBancaireDao) {
		this.compteBancaireDao = compteBancaireDao;
	}

	@Override
	public List<Personne> listeAdherent(Banque banque) {
//		Set<Personne> persons = banque.getMapCompteAPersonne().values().stream().collect(toSet());
		Set<Personne> persons = banque.getSetCompteBancaire().stream().map((CompteBancaire c) -> c.getTitulaire())
				.collect(toSet());
		return new ArrayList<>(persons);

	}

}
