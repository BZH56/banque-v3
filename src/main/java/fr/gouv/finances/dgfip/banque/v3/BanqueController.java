package fr.gouv.finances.dgfip.banque.v3;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import fr.gouv.finances.dgfip.banque.v3.dao.BanqueDao;
import fr.gouv.finances.dgfip.banque.v3.dao.PersonneDao;
import fr.gouv.finances.dgfip.banque.v3.entites.Banque;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteCourant;
import fr.gouv.finances.dgfip.banque.v3.entites.Personne;
import fr.gouv.finances.dgfip.banque.v3.services.BanqueServiceInterface;
import fr.gouv.finances.dgfip.banque.v3.services.PersonneServiceInterface;

@Controller
public class BanqueController implements WebMvcConfigurer {

//	private final Banque banque = new Banque("DGFiP");
	private final String nomBanque = "DGFiP";
	private final String codeBanque = "30005";

	@Autowired
	BanqueServiceInterface banqueService;

	@Autowired
	private BanqueDao banqueDao;

	@Autowired
	private PersonneDao personneDao;

	@Autowired
	PersonneServiceInterface personneService;

	@Autowired
	SystemeBancaireInterface systemeBancaireService;

	@Autowired
	BanqueController(BanqueDao banqueDaoArg) {
		banqueDaoArg.save(new Banque(this.nomBanque, this.codeBanque));
	}

	public BanqueController() {
		super();
		// TODO Auto-generated constructor stub
	}

	@RequestMapping("/")
	public String home(Model model) {
		Banque banque = banqueDao.findByNomBanque(this.nomBanque);
		model.addAttribute("nomBanque", banque.getNomBanque());
		model.addAttribute("adherents", systemeBancaireService.listeAdherent(banque));
		return "home";
	}

	@GetMapping("/add-current-account-full")
	public String addAccountFull(ModelMap model) {
		CompteCourant compteCourant = new CompteCourant();
//		compteCourant.setSolde(0.0);
		Personne personne = new Personne();
		model.addAttribute("personne", personne);
		model.addAttribute("compteCourant", compteCourant);
		Banque banque = banqueDao.findByCodeBanque(this.codeBanque);
		model.addAttribute("banque", banque);
		model.addAttribute("nomBanque", banque.getNomBanque());

		return "formAddCurrentAccountFull";
	}

	// NB: Don't dash for @ModelAttribute argument
	// @ModelAttribute("compteCourant") : OK
	// @ModelAttribute("compte-courant") : FAILED
	@PostMapping("/add-current-account-full")
	public String addAccountFullSubmit(@Valid @ModelAttribute("personne") Personne personne,
			BindingResult resultPersonne, @Valid @ModelAttribute("compteCourant") CompteCourant compteCourant,
			BindingResult resultCompte, Model model) {
		if (resultPersonne.hasErrors() || resultCompte.hasErrors()) {
			// In case of error in the form, you have to bind the banque again to the
			// model as it is not retrieve from the argument of this method.
			// Otherwise, you get an exception:
			// javax.servlet.jsp.JspTagException: Neither BindingResult nor plain
			// target object for bean name 'codeBanque' available as request attribute
			// System.out.println("model: " + model);
			Banque banque = banqueDao.findByCodeBanque(this.codeBanque);
			model.addAttribute("banque", banque);
			model.addAttribute("nomBanque", banque.getNomBanque());
			return "formAddCurrentAccountFull";
		}

		try {
			Personne personne1 = new Personne(personne.getNom(), personne.getPrenom());
			personneDao.save(personne1);
			Banque banque = banqueDao.findByCodeBanque(this.codeBanque);
			model.addAttribute("banque", banque);
			model.addAttribute("nomBanque", banque.getNomBanque());
			banqueService.creerCompteCourant(banque, personne1, compteCourant.getCodeGuichet(),
					compteCourant.getSolde());
			return "formResponseAddCurrentAccountFull";
		} catch (CompteException e) {
			// As both Personne and CompteCourant models are valided,
			// this catch should never happen.
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/add-current-account")
	public ModelAndView addAccount(@Valid @ModelAttribute("banque") Banque banque, Model model) {
		Banque banque1 = banqueDao.findByCodeBanque(this.codeBanque);
		model.addAttribute("banque", banque1);
		model.addAttribute("nomBanque", banque1.getNomBanque());
		return new ModelAndView("formAddCurrentAccount", "addCompteCourantForm", new AddCompteCourantForm());
	}

	@PostMapping("/add-current-account")
	public String addAccountSubmit(
			@Valid @ModelAttribute("addCompteCourantForm") AddCompteCourantForm compteCourantForm,
			BindingResult resultCompte, Model model) {
		Banque banque = banqueDao.findByCodeBanque(this.codeBanque);
		model.addAttribute("banque", banque);
		model.addAttribute("nomBanque", banque.getNomBanque());
		if (resultCompte.hasErrors()) {
			return "formAddCurrentAccount";
		}

		try {
			Personne personne = new Personne(compteCourantForm.getNom(), compteCourantForm.getPrenom());
			personneDao.save(personne);
			// Effectively create a current accompte
			model.addAttribute("banque", banque);
			model.addAttribute("nomBanque", banque.getNomBanque());
			CompteCourant compteCourant = banqueService.creerCompteCourant(banque, personne,
					compteCourantForm.getCodeGuichet());
			model.addAttribute("personne", personne);
			model.addAttribute("compteCourant", compteCourant);
			return "formResponseAddCurrentAccount";
		} catch (CompteException e) {
			// As both Personne and CompteCourant models are valided,
			// this catch should never happen.
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/synthese-compte")
	public String syntheseCompte(Model model, @ModelAttribute("error") String error) {
//		HashMap<CompteBancaire, Personne> mapCompteAPersonne = banque.getMapCompteAPersonne();
		Banque banque = banqueDao.findByNomBanque(this.nomBanque);
		Iterable<Banque> banques = banqueDao.findAll();
		Iterable<Personne> personnes = personneDao.findAll();
		Map<CompteBancaire, Personne> mapCompteAPersonne = new HashMap<CompteBancaire, Personne>();
		for (Banque b : banques) {
			for (CompteBancaire c : b.getSetCompteBancaire()) {
				mapCompteAPersonne.put(c, c.getTitulaire());
			}
			break;
		}
		model.addAttribute("error", error);
		model.addAttribute("compteAPersonne", mapCompteAPersonne);
		model.addAttribute("nomBanque", banque.getNomBanque());
		return "syntheseCompte";
	}

	// Documentation about redirection:
	// https://www.baeldung.com/spring-redirect-and-forward
	@GetMapping("/delete-account")
	public RedirectView deleteAccount(@RequestParam("codeGuichet") String codeGuichet,
			@RequestParam("numCompte") String numCompte, RedirectAttributes attributes) {
		Banque banque = banqueDao.findByCodeBanque(this.codeBanque);
		Boolean isDeleted = banque.supprimerCompte(codeGuichet, numCompte);
		if (!isDeleted) {
			attributes.addFlashAttribute("error", "Compte non supprimé.");
		}
		return new RedirectView("synthese-compte");
	}

}
