package fr.gouv.finances.dgfip.banque.v3.entites;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Banque {

	@Id
	@GeneratedValue // (strategy = GenerationType.IDENTITY)
	private UUID id;

	private String nomBanque;
	private String codeBanque;

	@OneToMany(mappedBy = "banque")
	private Set<CompteBancaire> setCompteBancaire = new HashSet<CompteBancaire>();
	@OneToMany(mappedBy = "banque")
	private Set<CarteBancaire> setCarte = new HashSet<CarteBancaire>();

//	private HashMap<CompteBancaire, Personne> mapCompteAPersonne = new HashMap<CompteBancaire, Personne>();

	private int numCompte = 0;

	/********************************/

	public Banque() {
		super();
		this.codeBanque = "";
		this.nomBanque = "";
	}

	public Banque(String nomBanque) {
		this.nomBanque = nomBanque;
	}

	public Banque(String nomBanque, String codeBanque) {
		this.nomBanque = nomBanque;
		this.codeBanque = codeBanque;
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the setCompteBancaire
	 */
	public Set<CompteBancaire> getSetCompteBancaire() {
		return setCompteBancaire;
	}

	/**
	 * @param setCompteBancaire the setCompteBancaire to set
	 */
	public void setSetCompteBancaire(Set<CompteBancaire> setCompteBancaire) {
		this.setCompteBancaire = setCompteBancaire;
	}

	/**
	 * @return the setCarte
	 */
	public Set<CarteBancaire> getSetCarte() {
		return setCarte;
	}

	/**
	 * @param setCarte the setCarte to set
	 */
	public void setSetCarte(Set<CarteBancaire> setCarte) {
		this.setCarte = setCarte;
	}

	/**
	 * @return the numCompte
	 */
	public int getNumCompte() {
		return numCompte;
	}

	/**
	 * @param numCompte the numCompte to set
	 */
	public void setNumCompte(int numCompte) {
		this.numCompte = numCompte;
	}

	/**
	 * @return the codeBanque
	 */
	public String getCodeBanque() {
		return codeBanque;
	}

	/********************************/

	/********************************/
	public void addCarte(CarteBancaire newCarteBancaire) {
		setCarte.add(newCarteBancaire);
	}

	/**
	 * @return the nomBanque
	 */
	public String getnomBanque() {
		return nomBanque;
	}

	/**
	 * @param nomBanque the nomBanque to set
	 */
	public void setnomBanque(String nomBanque) {
		this.nomBanque = nomBanque;
	}

	/**
	 * @param codeBanque the codeBanque to set
	 */
	public void setCodeBanque(String codeBanque) {
		this.codeBanque = codeBanque;
	}

//	public HashMap<CompteBancaire, Personne> getMapCompteAPersonne() {
//		return mapCompteAPersonne;
//	}

//
//	/**
//	 * @param mapCompteAPersonne the mapCompteAPersonne to set
//	 */
//	public void setMapCompteAPersonne(HashMap<CompteBancaire, Personne> mapCompteAPersonne) {
//		this.mapCompteAPersonne = mapCompteAPersonne;
//	}

	/**
	 * @return the nomBanque
	 */
	public String getNomBanque() {
		return nomBanque;
	}

	/**
	 * @param nomBanque the nomBanque to set
	 */
	public void setNomBanque(String nomBanque) {
		this.nomBanque = nomBanque;

	}

	public Boolean supprimerCompte(String codeGuichet, String numCompte) {

		Boolean res = false;
		System.out.println(codeGuichet + "/" + numCompte);

		CompteBancaire compteBancaireATraiter = compteBancaireDao.findByCodeGuichetAndNumCompte(codeGuichet, numCompte);

//		for (CompteBancaire compte : compteBancaireATraiter) {
//			if (compte.codeGuichet.equals(codeGuichet) && compte.numCompte.equals(numCompte)) {
//				mapCompteAPersonne.remove(compte);
//				res = true;
//				break;
//			}
//		}
		return res;
	}

}
