package fr.gouv.finances.dgfip.banque.v3.entites;

import javax.persistence.Entity;

import fr.gouv.finances.dgfip.banque.v3.CompteException;

@Entity
public class CompteEpargne extends CompteBancaire {

//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private UUID id;

	private Double txInteret;

	public CompteEpargne() {
		super();
		this.txInteret = 0.;
	}

	public CompteEpargne(String codeBanque, String codeGuichet, String numCompte, String cle, Double solde,
			Double txInteret) {
		super(codeBanque, codeGuichet, numCompte, cle, solde);
		this.txInteret = txInteret;
	}

	public Double calculerSolde() {
		Double solde = this.solde;
		for (int i = 0; i < this.operations.size(); i++) {
			solde += this.operations.get(i).getMontant();
		}
		return solde;
	}

	public Double calculerInterets() {
		return txInteret * calculerSolde();
	}

	public Integer creerOperation(String libelle, Double montant) throws CompteException {
		if (calculerSolde() + montant < 0) {
			throw new CompteException("Operation impossible : solde negatif");
		} else {
			return creerOperation(libelle, montant);
		}
	}
}
