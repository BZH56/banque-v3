package fr.gouv.finances.dgfip.banque.v3.entites;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

@Entity
public class Personne {

	@Id
	@GeneratedValue // (strategy = GenerationType.IDENTITY)
	private UUID id;

//	private final String nom;
//	private final String prenom;
	@NotBlank(message = "Nom ?")
	private String nom;
	@NotBlank(message = "Prénom ?")
	private String prenom;

	@OneToMany(mappedBy = "titulaire")
	private Set<CompteBancaire> setCompteBancaire = new HashSet<CompteBancaire>();
	@OneToMany(mappedBy = "titulaire")
	private Set<CarteBancaire> setCarte = new HashSet<CarteBancaire>();

	/********************************/

	public Personne() {
		super();
		this.nom = "";
		this.prenom = "";
	}

	public Personne(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}

	/********************************/
	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/********************************/
	public void addCompteBancaire(CompteBancaire newCompteBancaire) {
		setCompteBancaire.add(newCompteBancaire);
	}

	public void addCarte(CarteBancaire newCarteBancaire) {
		setCarte.add(newCarteBancaire);
	}

	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return the setCompteBancaire
	 */
	public Set<CompteBancaire> getSetCompteBancaire() {
		return setCompteBancaire;
	}

	/**
	 * @param setCompteBancaire the setCompteBancaire to set
	 */
	public void setSetCompteBancaire(Set<CompteBancaire> setCompteBancaire) {
		this.setCompteBancaire = setCompteBancaire;
	}

	/**
	 * @return the setCarte
	 */
	public Set<CarteBancaire> getSetCarte() {
		return setCarte;
	}

	/**
	 * @param setCarte the setCarte to set
	 */
	public void setSetCarte(Set<CarteBancaire> setCarte) {
		this.setCarte = setCarte;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}

}
