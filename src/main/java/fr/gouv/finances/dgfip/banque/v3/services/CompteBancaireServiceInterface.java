package fr.gouv.finances.dgfip.banque.v3.services;

import fr.gouv.finances.dgfip.banque.v3.CompteException;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteBancaire;
import fr.gouv.finances.dgfip.banque.v3.entites.CompteEpargne;

public interface CompteBancaireServiceInterface {

	public int creerOperation(CompteBancaire compte, String libelle, Double montant) throws CompteException;

	public void afficherSyntheseOperations(CompteBancaire compte);

	public double calculerInteret(CompteEpargne compte);

}
