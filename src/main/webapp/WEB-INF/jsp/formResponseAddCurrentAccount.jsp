<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>

<head>
<title>Compte courant cr��</title>
<style type="text/css">
.error {
    color: red;
    font-style: italic;
}
</style>
<c:url value="/css/main.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />
</head>

<body>
  <h1>BANQUE : ${banque.nomBanque}</h1>
  <div class="cadre400 mt50">
  	  <h2>Nouveau compte courant</h2>
  	  <div class="ligne120 ta-r mt5 va-t">
		Type compte : 
      </div>
      <div class="ligne250 ta-l mt5">
  	    Compte courant
  	  </div>
  	  
  	  <div class="ligne120 ta-r mt5 va-t">
		RIB :
      </div>
      <div class="ligne250 ta-l mt5">
  	    ${compteCourant.codeBanque} ${compteCourant.codeGuichet} ${compteCourant.numCompte} ${compteCourant.cle}
  	  </div>
  	  
      <div class="ligne120 ta-r mt5 va-t">
		Titulaire :
      </div>
      <div class="ligne250 ta-l mt5">
  	    ${personne.nom} ${personne.prenom}
  	  </div>
  	  
  	  <div class="ligne120 ta-r mt5 va-t">
		Solde :
      </div>
      <div class="ligne250 ta-l mt5">
  	    ${compteCourant.solde}
  	  </div>
   </div>
  <h3>
    <a href="/">Retour accueil</a>
  </h3>

</body>

</html>