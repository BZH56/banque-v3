<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html lang="en">
	<head>
<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />
	</head>
	<body>
  <h1>BANQUE : ${nomBanque}</h1>
        <a href="/synthese-compte"><button>Synth�se des comptes</button></a>
        <a href="/add-current-account"><button>Cr�er un compte courant</button></a>
        <a href="/add-current-account-full"><button>Cr�er un compte courant (full)</button></a>
        <div class="mt50"> 
			Nombre d'adh�rents : ${fn:length(adherents)}
	        <br />
	        <br />
	        <c:if test="${fn:length(adherents) != 0}">
	        <div class="cadre200">
		        <table style="width: 200px">
			        <tr>
			          <th style="font-weight: normal;">Nom</th>
			          <th style="font-weight: normal;">Pr�nom</th>
			        </tr>
		        	<c:forEach items="${adherents}" var="adherent">
				        <tr>
				          <td style="font-weight: normal;">
						     <c:out value="${adherent.nom}" />
						  </td>
				          <td style="font-weight: normal;">
				             <c:out value="${adherent.prenom}" />
				          </td>
				        </tr>
		        	</c:forEach>
		        </table>
	        </div>
	        </c:if>
		</div>
	</body>
</html>