<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>

<head>
<c:url value="/css/main.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />
<title>Nouveau compte courant</title>
<style type="text/css">
.error {
    color: red;
    font-style: italic;
}
</style>
</head>

<body>
  <h1>BANQUE : ${nomBanque}</h1>
  <div class="cadre400 mt50">
  	  <h2>Nouveau compte courant</h2>
	  <form action="add-current-account-full" method="POST">
	    <div>
          <spring:bind path="personne.nom">
	      <div class="ligne180 ta-r mt5 va-t">
	        <label>Nom : </label>
	      </div>
	      <div class="ligne200 ta-l mt5">
	        <input type="text" name="${status.expression}"
	          value="${status.value}"
	        >
	        <br />
	        <c:if test="${status.error}">
	          <span class="error">${status.errorMessages[0]}</span>
	        </c:if>
	        </div>
	      </spring:bind>
	    </div>
	    <br />
	
	    <div>
	      <spring:bind path="personne.prenom">
	      	<div class="ligne180 ta-r mt5 va-t">
	          <label>Pr�nom : </label>
	        </div>
	        <div class="ligne200 ta-l mt5">
		        <input type="text" name="${status.expression}" value="${status.value}">
		        <br />
		        <c:if test="${status.error}">
		          <span class="error">${status.errorMessages[0]}</span>
		        </c:if>
		     </div>   
	      </spring:bind>
	    </div>
	    <br />
	
	    <!-- 
	    This field is used only in the successful summary form
	    -->
	    <div>
	      <spring:bind path="banque.codeBanque">
	        <input type="hidden" name="${status.expression}"
	          value="${status.value}"
	        >
	      </spring:bind>
	    </div>
	
	    <div>
	      <spring:bind path="compteCourant.codeGuichet">
	        <div class="ligne180 ta-r mt5 va-t">
	          <label>Code guichet : </label>
	        </div>
	        <div class="ligne200 ta-l mt5">
	          <input type="text" name="${status.expression}" value="${status.value}">
	          <br />
	          <c:if test="${status.error}">
	            <span class="error">${status.errorMessages[0]}</span>
	          </c:if>
	        </div>
	      </spring:bind>
	    </div>
	    <br />
	
	    <div>
	      <spring:bind path="compteCourant.numCompte">
	        <div class="ligne180 ta-r mt5 va-t">
	          <label>Num�ro de compte : </label>
	        </div>
	        <div class="ligne200 ta-l mt5">
	          <input type="text" name="${status.expression}" value="${status.value}">
	          <br />
	          <c:if test="${status.error}">
	            <span class="error">${status.errorMessages[0]}</span>
	          </c:if>
	        </div>
	      </spring:bind>
	    </div>
	    <br />
	
	    <div>
	      <spring:bind path="compteCourant.cle">
	        <div class="ligne180 ta-r mt5 va-t">
	          <label>Cl� : </label>
	        </div>
	        <div class="ligne200 ta-l mt5">
	          <input type="text" name="${status.expression}" value="${status.value}">
	          <br />
	          <c:if test="${status.error}">
	            <span class="error">${status.errorMessages[0]}</span>
	          </c:if>
	        </div>
	      </spring:bind>
	    </div>
	    <br />
	
	    <div>
	      <spring:bind path="compteCourant.solde">
	        <div class="ligne180 ta-r mt5 va-t">
	          <label>Solde : </label>
	        </div>
	        <div class="ligne200 ta-l mt5">
	          <input type="text" name="${status.expression}" value="${status.value}" >
	          <br />
	          <c:if test="${status.error}">
	            <span class="error">${status.errorMessages[0]}</span>
	          </c:if>
	        </div>  
	      </spring:bind>
	    </div>
	    <div>
	    <br /> <input type="submit" value="Soumettre" />
	    </div>
	  </form>
  </div>
  <h3>
    <a href="/">Retour accueil</a>
  </h3>
</body>

</html>