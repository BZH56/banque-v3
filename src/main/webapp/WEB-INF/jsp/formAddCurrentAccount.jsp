<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>

<head>
<c:url value="/css/main.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />
<title>Nouveau compte courant</title>
<style type="text/css">
.error {
    color: red;
    font-style: italic;
}
</style>
</head>

<body>
  <h1>BANQUE : ${nomBanque}</h1>
  <div class="cadre400 mt50">
  	  <h2>Nouveau compte courant</h2>
  <sf:form action="add-current-account" method="POST"
    modelAttribute="addCompteCourantForm">
	    <div> 
	      <div class="ligne180 ta-r mt5 va-t">
			<sf:label path="nom">Nom : </sf:label>
	      </div>
	      <div class="ligne200 ta-l mt5">
			<sf:input path="nom" />
			<br />
			<sf:errors cssClass="error" path="nom" />
	      </div>
	    </div>
	    <br />
	    <div>
	      <div class="ligne180 ta-r mt5 va-t">
	        <sf:label path="prenom">Pr�nom : </sf:label>
	      </div>
	      <div class="ligne200 ta-l mt5">
	        <sf:input path="prenom" />
	        <br />
	        <sf:errors cssClass="error" path="prenom" />
	      </div>
	    </div>
	    <br />
	    <div>
	      <div class="ligne180 ta-r mt5 va-t">
	        <sf:label path="codeGuichet">Code guichet : </sf:label>
	      </div>
	      <div class="ligne200 ta-l mt5">
	        <sf:input path="codeGuichet" />
	        <br />
	        <sf:errors cssClass="error" path="codeGuichet" />
	      </div>
	    </div>
	    <br />
	    <div class="mt5">
	       <input type="submit" value="Soumettre" />
	    </div>
    </div>
  </sf:form>
  <h3>
     <a href="/">Retour accueil</a>
   </h3>
</body>

</html>